基本的に違うのはinheriteだけなので改変が有る場合そこだけ変える

niten
void Gene::inherit(Gene child1, Gene child2){
    int cutPosition1 = rand() % LENGTH;
    int cutPosition2 = rand() % LENGTH;
    if(cutPosition2 < cutPosition1) swap(cutPosition1, cutPosition2);

    for(int i = 0; i < cutPosition1; i ++) dna[i] = child1.dna[i];
    for(int i = cutPosition1; i < cutPosition2; i ++) dna[i] = child2.dna[i];
    for(int i = cutPosition2; i < LENGTH; i ++) dna[i] = child1.dna[i];
}

itiyo
void Gene::inherit(Gene child1, Gene child2){
    for(int i = 0; i < LENGTH; i ++){
        if(rand() % 2) dna[i] = child1.dna[i];
        else dna[i] = child2.dna[i];
    }
}
