#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <utility>
#include <algorithm>

#define mp make_pair
#define operation first.first
#define arg1 first.second
#define arg2 second.first
#define arg3 second.second

using namespace std;

typedef pair<int, int> pii;
typedef pair<pii, pii> data;

class Gene{
public:
    static const int LENGTH = 400;
    static const int NUMBER_OF_MODES = 6;
    
    Gene();
    Gene(const Gene&);
    void inherit(Gene, Gene);
    void mutate();
    vector<data> getDNA();
    void setDNA(vector<data>);
    data getRandomData();

private:
    vector<data> dna;
};


class TravelingTournament{
public:
	static const int N = 10; /**< チーム数*/
	static const int ROUND_MAX = 2 * (N - 1); /**< 何回戦まであるか*/
	static const int UNDEFINED = (int)1e7; /**< エラー時の距離*/
    static const int POPULATION = 1000; /**< 個体数 */
    static const int CROSSING_PROBABILITY = 80; /**< 交叉の確率（1/）*/
	static const int MUTATE_PROBABILITY = 2; /**< 突然変異の確率 (1/) */

	TravelingTournament(int (*)[N], int (*)[N]); /*!< コンストラクタ*/
	void run(void); /*!< ループ開始*/

private:
	int (*distance)[N]; /**< 距離行列のポインタ*/
	int (*defaultTournamentChart)[N]; /**< 初期の対戦表のポインタ*/
    int tournamentChart[ROUND_MAX][N]; /**< 作業用の対戦表*/
	int calculatedResult; /**< 距離の計算結果*/
    int generation; /**< 何世代目か*/

	int calculateByTournamentChart(void); /*!< 対戦表から距離を計算する*/
    int calculateByGene(Gene*); /*!< 遺伝子情報から距離を計算する*/
	void replaceHomeAndAway(int, int); /*!< ホームとアウェーを入れ替える*/
	void replaceRow(int, int); /*!< 日にちを入れ替える*/
	void replaceColumn(int, int); /*!< チームを入れ替える*/
	void partialReplaceRow(int, int, int); /*!< 日にちの一部を入れ替える*/
	void partialReplaceColumn(int, int, int); /*!< チームの一部を入れ替える*/
	bool isInsideRule(void); /*!< ルールを守ってるか判断する*/
    bool isRightAsTournamentChart(void);
	void out(void); /*!< コンソールに出力*/
    void initChart(void); /*!< 対戦表を初期化する*/
};


Gene::Gene(){
    dna.resize(LENGTH);

    for(int i = 0; i < LENGTH; i ++) dna[i] = getRandomData();
}

Gene::Gene(const Gene& obj){
    dna = obj.dna;
}

void Gene::inherit(Gene child1, Gene child2){
    for(int i = 0; i < LENGTH; i ++){
        if(rand() % 2) dna[i] = child1.dna[i];
        else dna[i] = child2.dna[i];
    }
}

void Gene::mutate(){
    int index = rand() % LENGTH;

    dna[index] = getRandomData();
}

vector<data> Gene::getDNA(){
    return dna;
}

void Gene::setDNA(vector<data> dat){
    dna = dat;
}

data Gene::getRandomData(){
    int ope = rand() % Gene::NUMBER_OF_MODES;
    int round1 = rand() % TravelingTournament::ROUND_MAX;
    int round2 = round1;
    while(round1 == round2) round2 = rand() % TravelingTournament::ROUND_MAX;
    int team1 = rand() % TravelingTournament::N;
    int team2 = team1;
    while(team1 == team2) team2 = rand() % TravelingTournament::N;

    data dat;
    switch(ope){
    case 0:
        dat = mp(mp(ope, team1), mp(team2, 0));
        break;
    case 1:
        dat = mp(mp(ope, round1), mp(round2, 0));
        break;
    case 2:
        dat = mp(mp(ope, team1), mp(team2, 0));
        break;
    case 3:
        dat = mp(mp(ope, round1), mp(round2, team1));
        break;
    case 4:
        dat = mp(mp(ope, team1), mp(team2, round1));
        break;
    case 5:
        dat = mp(mp(ope, 0), mp(0, 0));
        break;
    default:
        break;
    }

    return dat;
}


/**
  * TravelingTournamentクラスのコンストラクタ
  * 
  * @param dis 距離行列のポインタ
  * @param defaultTournamentChart 初期の対戦表
  */
TravelingTournament::TravelingTournament(int (*dis)[N], int (*defaultChart)[N]){
	distance = dis;
	defaultTournamentChart = defaultChart;

	if(!isInsideRule()){ calculatedResult = UNDEFINED; return; } 
	calculatedResult = calculateByTournamentChart();

    generation = 0;
}

/**
  * 最適解を求め走り出す
  */
void TravelingTournament::run(void){
    srand((unsigned)time(NULL));

    vector<Gene> nowGeneration(POPULATION);
    
    for(int i = 0; i < POPULATION; i ++)
        nowGeneration[i] = Gene();

    generation = 0;

    initChart();
    cout << generation << " "<< calculateByTournamentChart() << endl;

    int pre = 0;
    while(++ generation <= 1000){

        //並び替え処理
        vector<int> result;
        for(int n = 0; n < POPULATION; n ++)
            result.push_back(calculateByGene(&nowGeneration[n]));

        for(int i = 0; i < POPULATION / 2; i ++){
            vector<int>::iterator it = min_element(result.begin() + i, result.end());
            swap(nowGeneration[i], nowGeneration[it - result.begin()]);
        }

        int output = calculateByGene(&nowGeneration[0]);
        cout << generation << " " << output << endl;

        //次の世代はなんですか
        vector<Gene> nextGeneration(POPULATION);
        
        //確率分布
        vector<int> roulette;

        for(int i = 0; i < POPULATION; i ++){
            for(int j = 0; j < POPULATION / (i + 1); j ++){
                roulette.push_back(i);
            }
        }

        for(int i = 0; i < POPULATION / 2; i ++){
            int index1 = roulette[rand() % roulette.size()], index2 = index1;
            while(index1 == index2) index2 = roulette[rand() % roulette.size()];

            //交叉
            if(rand() % 100 < CROSSING_PROBABILITY){
                nextGeneration[2 * i].inherit(nowGeneration[index1], nowGeneration[index2]);
                nextGeneration[2 * i + 1].inherit(nowGeneration[index2], nowGeneration[index1]);
                continue;
            }

            //突然変異or生存
            else{
                for(int j = 0; j <= 1; j ++){
                    int ind = 2 * i + j;

                    nextGeneration[ind].inherit(nowGeneration[index1], nowGeneration[index1]);

                    //突然変異
                    if(!rand() % 100 < MUTATE_PROBABILITY){
                        nextGeneration[ind].mutate();
                    }

                    //生存
                    else ;
                }
            }
        }
        nowGeneration = nextGeneration;
    }
}

/**
  * 対戦表と、その場合の距離を出力する
  */
void TravelingTournament::out(void){
	for(int round = 0; round < ROUND_MAX; round ++){
		for(int index = 0; index < N; index ++){
			printf("%2d ", ((tournamentChart[round][index] % 2) ? 1:-1) * (tournamentChart[round][index] / 2 + 1));
		}
		cout << endl;
	}
}

void TravelingTournament::initChart(void){
    for(int index = 0; index < N; index ++)
        for(int round = 0; round < ROUND_MAX; round ++)
            tournamentChart[round][index] = defaultTournamentChart[round][index];
}

int TravelingTournament::calculateByGene(Gene* gene){
    initChart();

    vector<data> dna = gene->getDNA();

    for(int n = 0; n < Gene::LENGTH; n ++){
        for(int i = 0; i < 2; i ++){
            switch(dna[n].operation){
                case 0:
                    replaceHomeAndAway(dna[n].arg1, dna[n].arg2);
                    break;
                case 1:
                    replaceRow(dna[n].arg1, dna[n].arg2);
                    break;
                case 2:
                    replaceColumn(dna[n].arg1, dna[n].arg2);
                    break;
                case 3:
                    partialReplaceRow(dna[n].arg1, dna[n].arg2, dna[n].arg3);
                    break;
                case 4:
                    partialReplaceColumn(dna[n].arg1, dna[n].arg2, dna[n].arg3);
                    break;
                case 5:
                    break;
                default:
                    break;
            }
            if(calculateByTournamentChart() != UNDEFINED) break;
        }
    }
    gene->setDNA(dna);
    return calculateByTournamentChart();
}
    
/**
  * 対戦表から距離を計算する
  * 
  * @return 距離の計算結果
  */
int TravelingTournament::calculateByTournamentChart(void){
	if(!isInsideRule()) return UNDEFINED;
	int distanceSum = 0;
	for(int index = 0, preIndex; index < N; index ++){
		preIndex = index;
		for(int round = 0; round < ROUND_MAX; round ++){
			int current = tournamentChart[round][index];
			int currentIndex = (current % 2) ? index:(current / 2);

			distanceSum += distance[preIndex][currentIndex];
			preIndex = currentIndex;
		}
		distanceSum += distance[preIndex][index];
	}
	return distanceSum;
}

/**
  * 対戦表上の、2チームのホームとアウェーを入れ替える
  * 
  * @param team1 1チーム目のインデックス
  * @param team2 2チーム目のインデックス
  */
void TravelingTournament::replaceHomeAndAway(int team1, int team2){
	const int TEAM1_HOME = 2 * team1;
	const int TEAM1_AWAY = TEAM1_HOME + 1;
	const int TEAM2_HOME = 2 * team2;
	const int TEAM2_AWAY = TEAM2_HOME + 1;

	for(int round = 0; round < ROUND_MAX; round ++){
		if(tournamentChart[round][team1] == TEAM2_AWAY)
			tournamentChart[round][team1] = TEAM2_HOME;
		else if(tournamentChart[round][team1] == TEAM2_HOME)
			tournamentChart[round][team1] = TEAM2_AWAY;
		if(tournamentChart[round][team2] == TEAM1_AWAY)
			tournamentChart[round][team2] = TEAM1_HOME;
		else if(tournamentChart[round][team2] == TEAM1_HOME)
			tournamentChart[round][team2] = TEAM1_AWAY;
	}
}

/**
  * 対戦表上の、行（日にち）を交換する
  * 
  * @param round1 日にちの1つ目 (0〜ROUND_MAX)
  * @param round2 日にちの2つ目 (0〜ROUND_MAX)
  */
void TravelingTournament::replaceRow(int round1, int round2){
	for(int index = 0; index < N; index ++){
		int tmp = tournamentChart[round1][index];
		tournamentChart[round1][index] = tournamentChart[round2][index];
		tournamentChart[round2][index] = tmp;
	}
}

/**
  * 対戦表上の、列（チーム）を交換する
  *
  * @param team1 チームの1つ目（0〜N）
  * @param team2 チームの2つ目（0〜N）
  */
void TravelingTournament::replaceColumn(int team1, int team2){
	for(int round = 0; round < ROUND_MAX; round ++){
		if(tournamentChart[round][team1] / 2 == team2) continue;
		int tmp = tournamentChart[round][team1];
		tournamentChart[round][team1] = tournamentChart[round][team2];
		tournamentChart[round][team2] = tmp;
	}
	for(int index = 0; index < N; index ++){
		if(index == team1 || index == team2) continue;
		for(int round = 0; round < ROUND_MAX; round ++){
			if(tournamentChart[round][index] / 2 == team1)
				tournamentChart[round][index] = 2 * team2 + (tournamentChart[round][index] % 2);
			else if(tournamentChart[round][index] / 2 == team2)
				tournamentChart[round][index] = 2 * team1 + (tournamentChart[round][index] % 2);
		}
	}
}

/**
  * 対戦表上の、行（日にち）の一部を交換する
  *
  * @param round1 日にちの1つ目 (0〜ROUND_MAX)
  * @param round2 日にちの2つ目 (0〜ROUND_MAX)
  * @param team 一部に該当するチーム（0〜N）
  */
void TravelingTournament::partialReplaceRow(int round1, int round2, int targetTeam){
    int currentTeam = targetTeam;
	while(1){
        swap(tournamentChart[round1][currentTeam], tournamentChart[round2][currentTeam]);
        currentTeam = tournamentChart[round2][currentTeam] / 2;
        if(currentTeam == targetTeam) break;
        swap(round1, round2);
    }
}

/**
  * 対戦表上の、列（チーム）の一部を交換する
  *
  * @param team1 チームの1つ目（0〜N）
  * @param team2 チームの2つ目（0〜N）
  * @param round 一部に該当する日にち（0〜ROUND_MAX）
  */
void TravelingTournament::partialReplaceColumn(int team1, int team2, int targetRound){
    int currentRound = targetRound;
    while(1){
        int rival1 = tournamentChart[currentRound][team1] / 2;
        int rival2 = tournamentChart[currentRound][team2] / 2;
        swap(tournamentChart[currentRound][team1], tournamentChart[currentRound][team2]);
        swap(tournamentChart[currentRound][rival1], tournamentChart[currentRound][rival2]);
        if((tournamentChart[currentRound][team1] % 2) - (tournamentChart[currentRound][team2] % 2) != 0){
            tournamentChart[currentRound][rival1] += ((tournamentChart[currentRound][rival1] % 2) ? -1:1);
            tournamentChart[currentRound][rival2] += ((tournamentChart[currentRound][rival2] % 2) ? -1:1);
        }

        int preRound = currentRound;
        for(int round = 0; round < ROUND_MAX; round ++){
            if(tournamentChart[round][team1] == tournamentChart[currentRound][team1] && round != currentRound){
                currentRound = round;
                break;
            }
        }
        if(currentRound == preRound) break;
    }
}

/**
  * ルールを守っているかのチェック
  * 
  * return 守っている/いない (true/false)
  */
bool TravelingTournament::isInsideRule(void){
	//home/away連続のチェック
	//れんちゃんで当たってるかチェック
	for(int index = 0; index < N; index ++){
		int homeCount = 0, awayCount = 0, preIndex = index;
		for(int round = 0; round < ROUND_MAX; round ++){
			int current = tournamentChart[round][index];
			int currentIndex = current / 2;
			if(preIndex == currentIndex) return false;
			if(current % 2){
				homeCount ++;
				awayCount = 0;
			}
			else{
				awayCount ++;
				homeCount = 0;
			}
			if(homeCount == 4 || awayCount == 4) return false;

			preIndex = currentIndex;
		}
	}
	return true;
}


int main(void){

	//距離の表(x->距離, y->チーム)
	int distance[TravelingTournament::N][TravelingTournament::N];

	//対戦表 (x->チーム, y->日にち)
	int defaultTournamentChart[TravelingTournament::ROUND_MAX][TravelingTournament::N];

	//距離の表の入力
	for(int i = 0; i < TravelingTournament::N; i ++)
		for(int j = 0; j < TravelingTournament::N; j ++)
			cin >> distance[i][j];

	//対戦表の入力
	for(int i = 0; i < TravelingTournament::ROUND_MAX; i ++)
		for(int j = 0; j < TravelingTournament::N; j ++)
			cin >> defaultTournamentChart[i][j];

	//走らせよう
	TravelingTournament travelingTournament(distance, defaultTournamentChart);
	travelingTournament.run();


	return 0;
}
