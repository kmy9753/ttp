#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <utility>
#include <algorithm>

#define mp make_pair
#define operation first.first
#define arg1 first.second
#define arg2 second.first
#define arg3 second.second

using namespace std;

typedef pair<int, int> pii;
typedef pair<pii, pii> data;

class Gene{
public:
    static const int LENGTH = 400;
    static const int NUMBER_OF_MODES = 6;
    
    Gene();
    //Gene(int*);
    Gene(const Gene&);
    void inherit(Gene&, Gene&);
    void mutate();
    vector<data> getDNA();
    void setDNA(vector<data>&);
    data getRandomData();

private:
    vector<data> dna;
};


class TravelingTournament{
public:
	static const int N = 10; /**< チーム数*/
	static const int DAY_MAX = 2 * (N - 1); /**< 何回戦まであるか*/
	static const int UNDEFINED = (int)1e7; /**< エラー時の距離*/
    static const int POPULATION = 1000; /**< 個体数 */
    static const int CROSSING_PROBABILITY = 80; /**< 交叉の確率（1/）*/
	static const int MUTATE_PROBABILITY = 2; /**< 突然変異の確率 (1/) */

	TravelingTournament(int (*)[N], int (*)[N]); /*!< コンストラクタ*/
	void run(void); /*!< ループ開始*/

private:
	int (*distance)[N]; /**< 距離行列のポインタ*/
	int (*defaultTournamentChart)[N]; /**< 初期の対戦表のポインタ*/
    int tournamentChart[DAY_MAX][N]; /**< 作業用の対戦表*/
	int calculatedResult; /**< 距離の計算結果*/
    int generation; /**< 何世代目か*/

	int calculateByTournamentChart(void); /*!< 対戦表から距離を計算する*/
    int calculateByGene(Gene*); /*!< 遺伝子情報から距離を計算する*/
	void replaceHomeAndAway(int, int); /*!< ホームとアウェーを入れ替える*/
	void replaceRow(int, int); /*!< 日にちを入れ替える*/
	void replaceColumn(int, int); /*!< チームを入れ替える*/
	void partialReplaceRow(int, int, int); /*!< 日にちの一部を入れ替える*/
	void partialReplaceColumn(int, int, int); /*!< チームの一部を入れ替える*/
	bool isInsideRule(void); /*!< ルールを守ってるか判断する*/
    bool isRightAsTournamentChart(void);
	void out(void); /*!< コンソールに出力*/
    void initChart(void); /*!< 対戦表を初期化する*/
};


Gene::Gene(){
    dna.resize(LENGTH);

    for(int i = 0; i < LENGTH; i ++) dna[i] = getRandomData();
}

Gene::Gene(const Gene& obj){
    dna = obj.dna;
}

void Gene::inherit(Gene& child1, Gene& child2){
    for(int i = 0; i < LENGTH; i ++){
        if(rand() % 2) dna[i] = child1.dna[i];
        else dna[i] = child2.dna[i];
    }
}

void Gene::mutate(){
    int index = rand() % LENGTH;

    dna[index] = getRandomData();
}

vector<data> Gene::getDNA(){
    return dna;
}

void Gene::setDNA(vector<data>& dat){
    dna = dat;
}

data Gene::getRandomData(){
    int ope = rand() % Gene::NUMBER_OF_MODES;
    int day1 = rand() % TravelingTournament::DAY_MAX;
    int day2 = day1;
    while(day1 == day2) day2 = rand() % TravelingTournament::DAY_MAX;
    int team1 = rand() % TravelingTournament::N;
    int team2 = team1;
    while(team1 == team2) team2 = rand() % TravelingTournament::N;

    data dat;
    switch(ope){
    case 0:
        dat = mp(mp(ope, team1), mp(team2, 0));
        break;
    case 1:
        dat = mp(mp(ope, day1), mp(day2, 0));
        break;
    case 2:
        dat = mp(mp(ope, team1), mp(team2, 0));
        break;
    case 3:
        dat = mp(mp(ope, day1), mp(day2, team1));
        break;
    case 4:
        dat = mp(mp(ope, team1), mp(team2, day1));
        break;
    case 5:
        dat = mp(mp(ope, 0), mp(0, 0));
        break;
    }

    return dat;
}


/**
  * TravelingTournamentクラスのコンストラクタ
  * 
  * @param dis 距離行列のポインタ
  * @param defaultTournamentChart 初期の対戦表
  */
TravelingTournament::TravelingTournament(int (*dis)[N], int (*defaultChart)[N]){
	distance = dis;
	defaultTournamentChart = defaultChart;

	if(!isInsideRule()){ calculatedResult = UNDEFINED; return; } 
	calculatedResult = calculateByTournamentChart();

    generation = 0;
}

/**
  * 最適解を求め走り出す
  */
void TravelingTournament::run(void){
    srand((unsigned)time(NULL));

    vector<Gene> nowGeneration(POPULATION);
    
    for(int i = 0; i < POPULATION; i ++)
        nowGeneration[i] = Gene();

    generation = 0;

    initChart();
    cout << generation << " "<< calculateByTournamentChart() << endl;

    int pre = 0;
    while(++ generation <= 1000){

        //並び替え処理
        vector<int> result;
        for(int n = 0; n < POPULATION; n ++)
            result.push_back(calculateByGene(&nowGeneration[n]));

        for(int i = 0; i < POPULATION / 2; i ++){
            vector<int>::iterator it = min_element(result.begin() + i, result.end());
            swap(nowGeneration[i], nowGeneration[it - result.begin()]);
        }

        int output = calculateByGene(&nowGeneration[0]);
        
        cout << generation << " " << output << endl;
        if(pre != output){
            cout << "generation:" << generation<< " -> sum:" << output << endl;
            vector<data> a = nowGeneration[0].getDNA();
            for(int i = 0; i < Gene::LENGTH; i ++)
                cout << a[i].operation;
            cout << endl << endl;

            for(int i = 0; i < N; i ++){ for(int d = 0; d < DAY_MAX; d ++) printf("%2d ", tournamentChart[d][i] / 2 * ((tournamentChart[d][i] % 2) ? 1:-1)); cout << endl;} cout << endl;
        }
        else{
            for(int i = 0; i < POPULATION; i ++){
                nowGeneration[i] = Gene();
            }
            continue;
        }
        pre = output;
       //     cout << "sum:" << output << endl << endl;
        

        //次の世代はなんですか
        vector<Gene> nextGeneration(POPULATION);
        
        //パジェロ的な
        vector<int> roulette;

        for(int i = 0; i < POPULATION; i ++){
            //for(int j = 0; j < POPULATION * POPULATION / (i + 1); j ++){
            for(int j = 0; j < POPULATION / (i + 1); j ++){
                roulette.push_back(i);
            }
        }

        for(int i = 0; i < POPULATION / 2; i ++){
            int index1 = roulette[rand() % roulette.size()], index2 = index1;
            while(index1 == index2) index2 = roulette[rand() % roulette.size()];

            //交叉
            if(rand() % 100 < CROSSING_PROBABILITY){
                nextGeneration[2 * i].inherit(nowGeneration[index1], nowGeneration[index2]);
                nextGeneration[2 * i + 1].inherit(nowGeneration[index2], nowGeneration[index1]);
                continue;
            }

            //突然変異or生存
            else{
                for(int j = 0; j <= 1; j ++){
                    int ind = 2 * i + j;

                    nextGeneration[ind].inherit(nowGeneration[index1], nowGeneration[index1]);

                    //突然変異
                    if(!rand() % 100 < MUTATE_PROBABILITY){
                        nextGeneration[ind].mutate();
                    }

                    //生存
                    else ;
                }
            }
        }
        nowGeneration = nextGeneration;
    }
}

/**
  * 対戦表と、その場合の距離を出力する
  */
void TravelingTournament::out(void){
	for(int day = 0; day < DAY_MAX; day ++){
		for(int index = 0; index < N; index ++){
			printf("%2d ", ((tournamentChart[day][index] % 2) ? 1:-1) * (tournamentChart[day][index] / 2 + 1));
		}
		cout << endl;
	}
}

void TravelingTournament::initChart(void){
    for(int index = 0; index < N; index ++)
        for(int day = 0; day < DAY_MAX; day ++)
            tournamentChart[day][index] = defaultTournamentChart[day][index];
}

int TravelingTournament::calculateByGene(Gene* gene){
    initChart();

    //srand((unsigned)time(NULL));

    vector<data> dna = gene->getDNA();

    for(int n = 0; n < Gene::LENGTH; n ++){

        switch(dna[n].operation){
        case 0:
            replaceHomeAndAway(dna[n].arg1, dna[n].arg2);
            break;
        case 1:
            replaceRow(dna[n].arg1, dna[n].arg2);
            break;
        case 2:
            replaceColumn(dna[n].arg1, dna[n].arg2);
            break;
        case 3:
            partialReplaceRow(dna[n].arg1, dna[n].arg2, dna[n].arg3);
            break;
        case 4:
            partialReplaceColumn(dna[n].arg1, dna[n].arg2, dna[n].arg3);
            break;
        case 5:
            break;
        default:
            break;
        }
        if(calculateByTournamentChart() == UNDEFINED){

            switch(dna[n].operation){
                case 0:
                    replaceHomeAndAway(dna[n].arg1, dna[n].arg2);
                    break;
                case 1:
                    replaceRow(dna[n].arg1, dna[n].arg2);
                    break;
                case 2:
                    replaceColumn(dna[n].arg1, dna[n].arg2);
                    break;
                case 3:
                    partialReplaceRow(dna[n].arg1, dna[n].arg2, dna[n].arg3);
                    break;
                case 4:
                    partialReplaceColumn(dna[n].arg1, dna[n].arg2, dna[n].arg3);
                    break;
                case 5:
                    break;
                default:
                    break;
            }
            //dna[n].operation = 5;
        }
    }
    gene->setDNA(dna);
    return calculateByTournamentChart();
}
    
/**
  * 対戦表から距離を計算する
  * 
  * @return 距離の計算結果
  */
int TravelingTournament::calculateByTournamentChart(void){
	if(!isInsideRule()) return UNDEFINED;
	int distanceSum = 0;
	for(int index = 0, preIndex; index < N; index ++){
		preIndex = index;
		for(int day = 0; day < DAY_MAX; day ++){
			int current = tournamentChart[day][index];
			int currentIndex = (current % 2) ? index:(current / 2);

			distanceSum += distance[preIndex][currentIndex];
			preIndex = currentIndex;
		}
		distanceSum += distance[preIndex][index];
	}
	return distanceSum;
}

/**
  * 対戦表上の、2チームのホームとアウェーを入れ替える
  * 
  * @param team1 1チーム目のインデックス
  * @param team2 2チーム目のインデックス
  */
void TravelingTournament::replaceHomeAndAway(int team1, int team2){
	const int TEAM1_HOME = 2 * team1;
	const int TEAM1_AWAY = TEAM1_HOME + 1;
	const int TEAM2_HOME = 2 * team2;
	const int TEAM2_AWAY = TEAM2_HOME + 1;

	for(int day = 0; day < DAY_MAX; day ++){
		if(tournamentChart[day][team1] == TEAM2_AWAY)
			tournamentChart[day][team1] = TEAM2_HOME;
		else if(tournamentChart[day][team1] == TEAM2_HOME)
			tournamentChart[day][team1] = TEAM2_AWAY;
		if(tournamentChart[day][team2] == TEAM1_AWAY)
			tournamentChart[day][team2] = TEAM1_HOME;
		else if(tournamentChart[day][team2] == TEAM1_HOME)
			tournamentChart[day][team2] = TEAM1_AWAY;
	}
}

/**
  * 対戦表上の、行（日にち）を交換する
  * 
  * @param day1 日にちの1つ目 (0〜DAY_MAX)
  * @param day2 日にちの2つ目 (0〜DAY_MAX)
  */
void TravelingTournament::replaceRow(int day1, int day2){
	for(int index = 0; index < N; index ++){
		int tmp = tournamentChart[day1][index];
		tournamentChart[day1][index] = tournamentChart[day2][index];
		tournamentChart[day2][index] = tmp;
	}
}

/**
  * 対戦表上の、列（チーム）を交換する
  *
  * @param team1 チームの1つ目（0〜N）
  * @param team2 チームの2つ目（0〜N）
  */
void TravelingTournament::replaceColumn(int team1, int team2){
	for(int day = 0; day < DAY_MAX; day ++){
		if(tournamentChart[day][team1] / 2 == team2) continue;
		int tmp = tournamentChart[day][team1];
		tournamentChart[day][team1] = tournamentChart[day][team2];
		tournamentChart[day][team2] = tmp;
	}
	for(int index = 0; index < N; index ++){
		if(index == team1 || index == team2) continue;
		for(int day = 0; day < DAY_MAX; day ++){
			if(tournamentChart[day][index] / 2 == team1)
				tournamentChart[day][index] = 2 * team2 + (tournamentChart[day][index] % 2);
			else if(tournamentChart[day][index] / 2 == team2)
				tournamentChart[day][index] = 2 * team1 + (tournamentChart[day][index] % 2);
		}
	}
}

//こいつ動くぞ
/**
  * 対戦表上の、行（日にち）の一部を交換する
  *
  * @param day1 日にちの1つ目 (0〜DAY_MAX)
  * @param day2 日にちの2つ目 (0〜DAY_MAX)
  * @param team 一部に該当するチーム（0〜N）
  */
void TravelingTournament::partialReplaceRow(int day1, int day2, int targetTeam){
    int currentTeam = targetTeam;
	while(1){
//        out();
//        cout << "day1:" << day1 << " day2:" << day2 << " currentTeam:" << currentTeam << endl;
        swap(tournamentChart[day1][currentTeam], tournamentChart[day2][currentTeam]);
        currentTeam = tournamentChart[day2][currentTeam] / 2;
        if(currentTeam == targetTeam) break;
        swap(day1, day2);
    }
//    out();
//    cout <<  "PRR " << endl;
}

//う、うわああぁあぁぁああああ（椅子から転げ落ちる）
/**
  * 対戦表上の、列（チーム）の一部を交換する
  *
  * @param team1 チームの1つ目（0〜N）
  * @param team2 チームの2つ目（0〜N）
  * @param day 一部に該当する日にち（0〜DAY_MAX）
  */
void TravelingTournament::partialReplaceColumn(int team1, int team2, int targetDay){
//    if((tournamentChart[targetDay][team1] % 2) - (tournamentChart[targetDay][team2] % 2) != 0) return;

    int currentDay = targetDay;
    while(1){
        //out();
        //cout << "team1:" << team1 << " team2:" << team2 << " currentDay:" << currentDay << endl;
        int rival1 = tournamentChart[currentDay][team1] / 2;
        int rival2 = tournamentChart[currentDay][team2] / 2;
        swap(tournamentChart[currentDay][team1], tournamentChart[currentDay][team2]);
        swap(tournamentChart[currentDay][rival1], tournamentChart[currentDay][rival2]);
        if((tournamentChart[currentDay][team1] % 2) - (tournamentChart[currentDay][team2] % 2) != 0){
            tournamentChart[currentDay][rival1] += ((tournamentChart[currentDay][rival1] % 2) ? -1:1);
            tournamentChart[currentDay][rival2] += ((tournamentChart[currentDay][rival2] % 2) ? -1:1);
        }

        int preDay = currentDay;
        for(int day = 0; day < DAY_MAX; day ++){
            if(tournamentChart[day][team1] == tournamentChart[currentDay][team1] && day != currentDay){
                currentDay = day;
                break;
            }
        }
        if(currentDay == preDay) break;
    }
//    out();
//    cout <<  "PRC " << endl;
}

//bool TravelingTournament::isRightAsTournamentChart(void){
//    for(int day = 0; day < DAY_MAX; day ++){
//        for(int index = 0; index < N; index ++){
//            int rivalIndex = tournamentChart[day][index] / 2;
//            if(tournamentChart[day][rivalIndex] / 2 != index) return false;
//        }
//    }
//
//    return true;
//}

/**
  * ルールを守っているかのチェック
  * 
  * return 守っている/いない (true/false)
  */
bool TravelingTournament::isInsideRule(void){
	//home/away連続のチェック
	//れんちゃんで当たってるかチェック
	for(int index = 0; index < N; index ++){
		int homeCount = 0, awayCount = 0, preIndex = index;
		for(int day = 0; day < DAY_MAX; day ++){
			int current = tournamentChart[day][index];
			int currentIndex = current / 2;
			if(preIndex == currentIndex) return false;
			if(current % 2){
				homeCount ++;
				awayCount = 0;
			}
			else{
				awayCount ++;
				homeCount = 0;
			}
			if(homeCount == 4 || awayCount == 4) return false;

			preIndex = currentIndex;
		}
	}
	return true;
}


int main(void){

	//距離の表(x->距離, y->チーム)
	int distance[TravelingTournament::N][TravelingTournament::N];

	//対戦表 (x->チーム, y->日にち)
	int defaultTournamentChart[TravelingTournament::DAY_MAX][TravelingTournament::N];

	//距離の表の入力
	for(int i = 0; i < TravelingTournament::N; i ++)
		for(int j = 0; j < TravelingTournament::N; j ++)
			cin >> distance[i][j];

	//対戦表の入力
	for(int i = 0; i < TravelingTournament::DAY_MAX; i ++)
		for(int j = 0; j < TravelingTournament::N; j ++)
			cin >> defaultTournamentChart[i][j];

	//走らせよう
	TravelingTournament travelingTournament(distance, defaultTournamentChart);
	travelingTournament.run();


	return 0;
}
