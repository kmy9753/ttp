#include <iostream>
#include <ctime>
#include <cstdlib>
#include <algorithm>

using namespace std;

typedef long long ll;

static const int UNANALYZED = 0;

ll factorial(ll a){
	if(a == 1) return a;
	return a * factorial(a - 1);
}

int main(void){

	//チーム数
	ll n; cin >> n;

	//日にち
	const int DAY_MAX = 2 * (n - 1);

	//対戦表.
	int permutation[n];
	int tournamentChart[DAY_MAX][n];
	bool homeMap[n][n];

	for(int index = 0; index < n; index ++){
		permutation[index] = 2 * index;

		for(int index2 = 0; index2 < n; index2 ++)
			homeMap[index][index2] = false;
		for(int day = 0; day < DAY_MAX; day ++)
			tournamentChart[day][index] = UNANALYZED;
	}

	/**走る**/
	//ホームとアウェーを固定した場合を列挙
	for(int day = 0; day < DAY_MAX / 2; day ++){

		//順列を基に対戦表を埋める
		for(int i = 0; i < n; i += 2){
			tournamentChart[day][permutation[i] / 2] = permutation[i + 1];
			tournamentChart[day][permutation[i + 1] / 2] = permutation[i] + 1;
			homeMap[permutation[i] / 2][permutation[i + 1] / 2] = true;

		}

		//順列をごにょごにょする
		for(int i = 1, nextIndex = 0, swap = permutation[i], swapNext; nextIndex != 1 ; i = nextIndex){
			//nextIndexはどこだ
			if(i % 2)
				nextIndex = (i + 2 < n) ? (i + 2):(i - 1);
			else
				nextIndex = (0 < i - 2) ? (i - 2):(i - 1);
			
			swapNext = permutation[nextIndex];
			permutation[nextIndex] = swap;
			swap = swapNext;
		}
	}

	//ホームとアウェーを入れ替えた場合を列挙
	for(int day = DAY_MAX / 2; day < DAY_MAX; day ++){

		//順列を基に対戦表を埋める
		for(int i = 0; i < n; i += 2){
			tournamentChart[day][permutation[i] / 2] = permutation[i + 1] + ((homeMap[permutation[i] / 2][permutation[i + 1] / 2]) ? 1:0);
			tournamentChart[day][permutation[i + 1] / 2] = permutation[i] + ((homeMap[permutation[i] / 2][permutation[i + 1] / 2]) ? 0:1);

		}

		//順列をごにょごにょする
		for(int i = 1, nextIndex = 0, swap = permutation[i], swapNext; nextIndex != 1 ; i = nextIndex){
			//nextIndexはどこだ
			if(i % 2)
				nextIndex = (i + 2 < n) ? (i + 2):(i - 1);
			else
				nextIndex = (0 < i - 2) ? (i - 2):(i - 1);
			
			swapNext = permutation[nextIndex];
			permutation[nextIndex] = swap;
			swap = swapNext;
		}
	}
	//出力
	for(int day = 0; day < DAY_MAX; day ++, cout << endl){
		for(int index = 0; index < n; index ++)
			cout << tournamentChart[day][index] << " ";
	}

	return 0;
}
